![Nationale Forschungsdateninfrastruktur (NFDI) Logo](https://gitlab.com/nfdi-de/nfdi-de/-/raw/main/images/nfdi-logo_600px.png)

# Welcome to NFDI on GitLab!


## NFDI consortia on GitLab.com

* [NFDI4Cat](https://gitlab.com/nfdi4cat)
* [NFDI4Culture](https://gitlab.com/nfdi4culture/)

## NFDI consortia on institutional GitLab Instances

* [NFDI4Earth](https://git.rwth-aachen.de/nfdi4earth)
* [NFDI4Ing](https://git.rwth-aachen.de/nfdi4ing)
* [nfdi-matwerk](https://git.rwth-aachen.de/nfdi-matwerk)
* [NFDI4Plants](https://git.nfdi4plants.org/explore)

## see also

* [NFDI on GitHub](https://github.com/nfdi-de) 
